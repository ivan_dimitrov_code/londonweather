package com.puzzlegramma.londonweather.ui.main

import com.puzzlegramma.londonweather.ui.main.api.ApiClient
import com.puzzlegramma.londonweather.ui.main.models.external.CityLocationInfoResponse
import com.puzzlegramma.londonweather.ui.main.models.external.WeatherRequestResponse
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import retrofit2.Response

@RunWith(JUnit4::class)
class WeatherRepositoryTest {

    lateinit var repository: WeatherRepository

    @Mock
    lateinit var apiService: ApiClient

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        repository = WeatherRepository(apiService)
    }

    @Test
    fun `get location test`() {
        runBlocking {
            Mockito.`when`(apiService.getCityLocationInformation("london"))
                .thenReturn(Response.success(listOf<CityLocationInfoResponse>()))
            val response = repository.getCityLocationInformation("london")
            assertEquals(listOf<CityLocationInfoResponse>(), response.body())
        }
    }

    @Test
    fun `get weather for location`() {
        runBlocking {
            val responseMock = WeatherRequestResponse("london", emptyList())
            Mockito.`when`(apiService.getCityWeatherForId("1"))
                .thenReturn(Response.success(responseMock))
            val response = repository.getCityWeather("1")
            assertEquals(responseMock, response.body())
        }
    }
}
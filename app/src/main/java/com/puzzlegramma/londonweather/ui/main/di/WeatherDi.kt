package com.puzzlegramma.londonweather.ui.main.di

import com.puzzlegramma.londonweather.ui.main.WeatherRepository
import com.puzzlegramma.londonweather.ui.main.WeatherViewModel
import com.puzzlegramma.londonweather.ui.main.api.ApiClient
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val weatherDI = module {
    viewModel { WeatherViewModel(get()) }

    fun provideRepository(api: ApiClient): WeatherRepository {
        return WeatherRepository(api)
    }

    single { provideRepository(get()) }

    single { ApiClient.getInstance() }
}

package com.puzzlegramma.londonweather.ui.main

import android.app.Application
import com.puzzlegramma.londonweather.ui.main.di.weatherDI
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class AppApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        setupKoin()
    }

    private fun setupKoin() {
        startKoin {
            androidContext(this@AppApplication)
            modules(
                listOf(
                    weatherDI
                )
            )
        }
    }
}
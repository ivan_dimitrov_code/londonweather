package com.puzzlegramma.londonweather.ui.main.models.external

import com.google.gson.annotations.SerializedName

data class CityLocationInfoResponse(
    @SerializedName("woeid")
    val cityId: String
)
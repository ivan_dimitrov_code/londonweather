package com.puzzlegramma.londonweather.ui.main

import com.puzzlegramma.londonweather.ui.main.api.ApiClient

open class WeatherRepository constructor(private val retrofitService: ApiClient) {

    suspend fun getCityLocationInformation(cityName: String) =
        retrofitService.getCityLocationInformation(cityName)

    suspend fun getCityWeather(cityId: String) =
        retrofitService.getCityWeatherForId(cityId)
}
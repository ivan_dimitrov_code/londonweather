package com.puzzlegramma.londonweather.ui.main.models.external

import com.google.gson.annotations.SerializedName
import com.puzzlegramma.londonweather.ui.main.models.internal.CityWeatherInfo

data class WeatherRequestResponse(
    @SerializedName("title")
    val title: String,
    @SerializedName("consolidated_weather")
    val weather: List<CityWeatherInfoResponse>
)

fun WeatherRequestResponse.toCityWeatherInfo(): CityWeatherInfo {
    val iconUrl =
        "https://www.metaweather.com/static/img/weather/png/64/${weather[0].weatherCode}.png"
    return CityWeatherInfo(
        title,
        weather[0].weatherCode,
        weather[0].weatherState,
        weather[0].airPressure,
        weather[0].humidity,
        iconUrl,
        weather[0].temp
    )
}
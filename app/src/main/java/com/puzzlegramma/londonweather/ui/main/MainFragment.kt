package com.puzzlegramma.londonweather.ui.main

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.puzzlegramma.londonweather.databinding.MainFragmentBinding
import com.puzzlegramma.londonweather.ui.main.util.NetworkState
import com.squareup.picasso.Picasso
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainFragment : Fragment() {

    private val viewModel by viewModel<WeatherViewModel>()

    private lateinit var viewBinding: MainFragmentBinding

    companion object {
        fun newInstance() = MainFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewBinding = MainFragmentBinding.inflate(inflater, container, false)
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupDataObservation()
        viewBinding.retryButton.setOnClickListener {
            attemptToShowWeatherData()
        }
        attemptToShowWeatherData()
    }

    private fun setupDataObservation() {
        viewModel.loadingLiveData.observe(viewLifecycleOwner) {
            enterLoadingState()
        }

        viewModel.cityWeatherInfoLiveData.observe(viewLifecycleOwner) {
            viewBinding.cityName.text = it.title
            viewBinding.cityWeatherCondition.text = it.weatherState
            Picasso.get().load(it.iconUrl).into(viewBinding.imageView)
            viewBinding.cityTemp.text = "${it.temp} ℃"
            enterSuccessState()
        }

        viewModel.errorMessageLiveData.observe(viewLifecycleOwner) {
            enterErrorState()
        }
    }

    private fun attemptToShowWeatherData() {
        context?.let {
            if (NetworkState.isConnectedToNetwork(it)) {
                viewModel.getWeatherInfoFor("london")
            } else {
                enterErrorState()
            }
        }
    }

    private fun enterLoadingState() {
        viewBinding.errorGroup.visibility = View.INVISIBLE
        viewBinding.weatherLayout.visibility = View.INVISIBLE
        viewBinding.loadingIndicator.visibility = View.VISIBLE
    }

    private fun enterSuccessState() {
        viewBinding.errorGroup.visibility = View.INVISIBLE
        viewBinding.weatherLayout.visibility = View.VISIBLE
        viewBinding.loadingIndicator.visibility = View.INVISIBLE

    }

    private fun enterErrorState() {
        viewBinding.errorGroup.visibility = View.VISIBLE
        viewBinding.weatherLayout.visibility = View.INVISIBLE
        viewBinding.loadingIndicator.visibility = View.INVISIBLE
    }
}
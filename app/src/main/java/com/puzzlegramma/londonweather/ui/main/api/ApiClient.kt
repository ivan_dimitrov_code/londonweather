package com.puzzlegramma.londonweather.ui.main.api

import com.puzzlegramma.londonweather.ui.main.models.external.CityLocationInfoResponse
import com.puzzlegramma.londonweather.ui.main.models.external.WeatherRequestResponse
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


interface ApiClient {

    @GET("/api/location/search/")
    suspend fun getCityLocationInformation(@Query("query") cityName: String): Response<List<CityLocationInfoResponse>>

    @GET("/api/location/{locationID}/")
    suspend fun getCityWeatherForId(@Path("locationID") locationID: String): Response<WeatherRequestResponse>

    companion object {
        var retrofitService: ApiClient? = null
        fun getInstance(): ApiClient {
            val interceptor = HttpLoggingInterceptor()
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
            val client: OkHttpClient = OkHttpClient.Builder().addInterceptor(interceptor).build()

            if (retrofitService == null) {
                val retrofit = Retrofit.Builder()
                    .baseUrl("https://www.metaweather.com/")
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                retrofitService = retrofit.create(ApiClient::class.java)
            }
            return retrofitService!!
        }
    }
}
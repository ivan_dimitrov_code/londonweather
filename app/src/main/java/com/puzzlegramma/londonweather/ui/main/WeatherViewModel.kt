package com.puzzlegramma.londonweather.ui.main

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.puzzlegramma.londonweather.ui.main.models.external.CityWeatherInfoResponse
import com.puzzlegramma.londonweather.ui.main.models.external.toCityWeatherInfo
import com.puzzlegramma.londonweather.ui.main.models.internal.CityWeatherInfo
import kotlinx.coroutines.*

open class WeatherViewModel constructor(private val mainRepository: WeatherRepository) : ViewModel() {

    val errorMessageLiveData = MutableLiveData<String>()
    val cityWeatherInfoLiveData = MutableLiveData<CityWeatherInfo>()
    val loadingLiveData = MutableLiveData<Boolean>()
    var job: Job? = null

    fun getWeatherInfoFor(cityName: String) {
        job = CoroutineScope(Dispatchers.IO).launch {
            loadingLiveData.postValue(true)
            val response = mainRepository.getCityLocationInformation(cityName)
            val isResponseEmpty = response.body()?.isEmpty() ?: true
            if (response.isSuccessful || !isResponseEmpty) {
                val weatherResponse =
                    response.body()?.get(0)?.cityId?.let { mainRepository.getCityWeather(it) }
                if (weatherResponse?.isSuccessful == true) {
                    onSuccess(weatherResponse.body()?.toCityWeatherInfo())
                } else {
                    onError("Error : ${weatherResponse?.message()} ")
                }
            } else {
                onError("Error : ${response.message()} ")
            }
        }
    }

    private suspend fun onSuccess(cityWeatherInfo: CityWeatherInfo?) {
        withContext(Dispatchers.Main) {
            loadingLiveData.value = false
            cityWeatherInfo?.let { cityWeatherInfoLiveData.postValue(it) }
        }
    }

    private suspend fun onError(message: String) {
        withContext(Dispatchers.Main) {
            errorMessageLiveData.value = message
            loadingLiveData.value = false
        }
    }

    override fun onCleared() {
        super.onCleared()
        job?.cancel()
    }
}
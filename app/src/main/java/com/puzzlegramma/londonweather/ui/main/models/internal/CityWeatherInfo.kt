package com.puzzlegramma.londonweather.ui.main.models.internal

data class CityWeatherInfo(
    val title: String,
    val weatherCode: String,
    val weatherState: String,
    val airPressure: String,
    val humidity: String,
    val iconUrl: String,
    val temp: String
)

package com.puzzlegramma.londonweather.ui.main.models.external

import com.google.gson.annotations.SerializedName

data class CityWeatherInfoResponse(
    @SerializedName("weather_state_abbr")
    val weatherCode: String,

    @SerializedName("weather_state_name")
    val weatherState: String,

    @SerializedName("air_pressure")
    val airPressure: String,

    @SerializedName("humidity")
    val humidity: String,

    @SerializedName("the_temp")
    val temp: String
)
# README #

### Summanry ###

This is a simple weather app that uses the https://www.metaweather.com/api/

Architecture: 

* MVVM as architecture in combination with LiveData in the ViewModel. 

* Croutines + Retrofit for networking requests.

* GSON for Serialization/Deserialization.

* Koin for dependency injection.

* Picasso for image loading.

* Mockito for unit testing.


### App UI ###

App has 3 basic states: 

* Loading state - A loading indicator is shown. Active during the request response wait.
* Error state - A error message and a retry button is shown. Active during server error or network down.
* Success state - Weather information is shown. Active during successfull server call.

### App workflow ###

Happy path: 

* App makes request for the london location information to get the ID of london in the API.
* With that ID it makes a request for the london weather and it displays it.

Error path: 

* App shows the error state and upon pressing the retry button tries to make the request again